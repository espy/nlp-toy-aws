import boto3
import json
import csv
import os

# play around with these demo limits. Should be much higher, but
# when experimenting it can help control costs significantly
MAX_NOTES = 10
IN_DIR = 'input'
IN_FILE = '/data/mtsamples.csv'

trace = True
debug = True
info = True

split_start = 0
split_end = 10

def begin():
    if debug: print("Start " + str(split_start) + ':' + str(split_end))

    # uncomment this line to make json files to use as input for
    # nlp, with only {'1', 'Text note here'}. This can be done once
    # and the results committed to speed up processing.
    # runtime for 4000 input csv rows is about 10-20 sec
    make_input_files()
    
    # uncommend this linw to create result json files for all input
    # json files. Each request takes a few seconds, so this does
    # not scale great. Batching is called for.
    handle_input_files()

    if debug: print("End " + str(split_start) + ':' + str(split_end))


def make_input_files():
    file_name = IN_FILE
    file_line_pre_list = read_note_csv(file_name)
    file_line_list = file_line_pre_list

    if debug: print('found file line list len = ' + str(len(file_line_list)))
    result_map = make_string_map_from_row_list(file_line_list, MAX_NOTES)
    if debug: print('found result map len = ' + str(len(result_map.keys())))
    write_map_to_files(result_map, IN_DIR)

def read_note_csv(file_name):
    lines = []
    with open(file_name,'r') as data: 
        for line in csv.reader(data): 
            lines.insert(len(lines), line)
    return lines[1:]

def make_string_map_from_row_list(row_list_in, max_rows):
    result_map = {}
    row_count = len(row_list_in)

    row_list = row_list_in


    if len(row_list) < max_rows:
        max_rows = len(row_list)

    for i in range(0, max_rows):
        # if i < 5: print(str(row_list[i])[:111]) 
        # if i > max_rows-5: print(str(row_list[i])[:111])
        this_txt = (row_list[i][4]).strip()
        this_id = row_list[i][0]
        this_domain = (row_list[i][2]).strip()
        data_map = {'text': this_txt, 'domain':this_domain}
        result_map[this_id] = data_map
    return result_map

def write_map_to_files(map_in, prefix):
    keys_pre_list = list(map_in.keys())

    keys_list = keys_pre_list
    for key in keys_list:
        j = str(key)
        file_name = "/data/" + prefix + "/" + prefix + "-" + key + ".json"

        tmp_dict = {}
        tmp_dict[key] = map_in[key]

        f = open(file_name, "w")
        f.writelines(json.dumps(tmp_dict, indent=3, sort_keys=True))
        f.close()

def handle_input_files():
    path = '/data/' + IN_DIR
    file_pre_list = get_file_list()
    if file_pre_list:
        file_sorted_list = sorted(file_pre_list)
        file_list = file_sorted_list[split_start:split_end]
        for file_name in sorted(file_list):
            print(file_name)
            with open(path + '/' + file_name,'r') as data: 
                result_map = {}
                a = json.load(data)
                key = next(iter(a.keys()))
                data_map = a[key]
                e = get_entities_for_text(data_map.get('text'))
                new_data_map = { 'id': key, 'domain':data_map.get('domain'), 'entities':e }
                result_map[key] = new_data_map
                write_map_to_files(result_map, 'result')

def get_file_list():
    path = '/data/' + IN_DIR
    files = os.listdir(path)
    file_list = []

    for fp in files:
        if fp.endswith(".json"):
            file_list.insert(0,fp)
    return file_list

def get_entities_for_text(text_in):
    entity_list = []

    if text_in == None: return []
    if len(text_in) < 1: return []

    client = boto3.client(service_name='comprehendmedical', region_name='us-east-1')
    result = client.detect_entities(Text= text_in)
    entities = result['Entities']
    i=0
    for entity in entities:
        new_entity = entity
        new_entity['Id'] = i
        entity_list.insert(i, new_entity)
        i += 1

    return entity_list

def main():
    begin()

if __name__ == '__main__':
    begin()