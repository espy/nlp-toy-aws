import boto3
import json
import csv
import os

MAX_NOTES = 10
IN_DIR = 'input'
NLP_DIR = 'result'
IN_FILE = '/data/mtsamples.csv'

# def make_input_files():
#     file_name = IN_FILE
#     file_line_list = read_note_csv(file_name)
#     result_map = make_string_map_from_row_list(file_line_list, MAX_NOTES)
#     write_map_to_files(result_map, IN_DIR)

# def read_note_csv(file_name):
#     lines = []
#     with open(file_name,'r') as data: 
#         for line in csv.reader(data): 
#             lines.insert(len(lines), line)
#     return lines[1:]

# def make_string_map_from_row_list(row_list, max_rows):
#     result_map = {}
#     row_count = len(row_list)

#     if len(row_list) < max_rows:
#         max_rows = len(row_list)

#     for i in range(0, max_rows):
#         # if i < 5: print(str(row_list[i])[:111]) 
#         # if i > max_rows-5: print(str(row_list[i])[:111])
#         this_txt = row_list[i][4]
#         this_id = row_list[i][0]
#         this_domain = row_list[i][2]
#         result_map[this_id] = this_txt
#     return result_map

def write_map_to_files(map_in, file_name):
    # for key in map_in.keys():
        # j = str(key)
        file_name = "/data/" + file_name

        # tmp_dict = {}
        # tmp_dict[key] = map_in[key]

        f = open(file_name, "w")
        f.writelines(json.dumps(map_in, indent=3, sort_keys=True))
        f.close()

def read_nlp_out_files():
    path = '/data/' + NLP_DIR
    file_list = get_nlp_file_list()
    data_map = {}
    if file_list:
        for file_name in sorted(file_list):
            # print(file_name)
            with open(path + '/' + file_name,'r') as data: 
                file_map = {}
                a = json.load(data)
                key = next(iter(a.keys()))
                data_map[key] = a.get(key)

        write_map_to_files(data_map, 'all.json')
    return data_map

def get_nlp_file_list():
    path = '/data/' + NLP_DIR
    files = os.listdir(path)
    file_list = []

    for fp in files:
        if fp.endswith(".json"):
            file_list.insert(0,fp)
    return file_list

def do_counts(in_map):
    pass
    keys = in_map.keys()
    print('key count = ' + str(len(keys)))
    domain_map = {}

    for key in keys:
        domain = in_map.get(key).get('domain')
        entity_count = len(in_map[key]['entities'])
        if domain in domain_map:
            i = domain_map[domain]['domain-count']
            old_entity_count = domain_map[domain]['entity-count']
            domain_map[domain]['domain-count'] = i + 1
            domain_map[domain]['entity-count'] = entity_count + old_entity_count
        else:
            data_map = {'domain-count':1, 'entity-count': entity_count}
            domain_map[domain] = data_map

    csv_keys=domain_map.keys()
    new_result_list = []
    for ckey in csv_keys:
        row = domain_map[ckey]
        d = ckey
        c = row['domain-count']
        e = row['entity-count']
        new_string = '' + str(d) + ', ' + str(c) + ', ' + str(e) + '\n'
        new_result_list.append(new_string)

    write_csv_from_list(new_result_list)
    write_json_from_map(domain_map)
    return domain_map

def write_json_from_map(domain_map):
    file_name='summary.json'
    file_name = '/data/' + file_name
    f = open(file_name, "w")
    f.writelines(json.dumps(domain_map, indent=3, sort_keys=True))
    f.close()

def write_csv_from_list(string_list):
    file_name='summary.csv'
    file_name = '/data/' + file_name
    f = open(file_name, "w")
    f.writelines(string_list)
    f.close()

def begin():

    # uncomment this line to ...
    data_map = read_nlp_out_files()

    # uncomment this line to ...

    do_counts(data_map)
    # uncommend this like to create result json files for all input
    # json files. Each request takes a few seconds, so this does
    # not scale great.
    # handle_input_files()


    # text_list = result_map.values()
    # entities = get_entities_for_text_list(text_list)
    # print(str(len(entities)))
    # print(json.dumps(entities))

def main():
    begin()

if __name__ == '__main__':
    begin()