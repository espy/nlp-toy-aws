#!/usr/bin/env bash
#
# PWD="$(pwd)"
#
NOM="aws"
IMG="espy/aws"
OPTS="${OPTS} -it"
OPTS="${OPTS} --rm"
# OPTS="${OPTS} -e AZURE_TEXT_ANALYTICS_ENDPOINT=https://ta-nlp-1.cognitiveservices.azure.com"
# OPTS="${OPTS} -e AZURE_TEXT_ANALYTICS_KEY=erfghjkldfghjdfgh"
OPTS="${OPTS} --name ${NOM}1"
OPTS="${OPTS} -v ${PWD}/data:/data"
OPTS="${OPTS} -v ${HOME}/.aws:/root/.aws"
OPTS="${OPTS} -v $(pwd):/aws"

#
mkdir -p data/input
mkdir -p data/result

docker run ${OPTS} ${IMG} python3 /data/discover.py

