#!/usr/bin/env bash

NOM="espy/aws"
OPTS=" "
OPTS="${OPTS} --file Dockerfile "
OPTS="${OPTS} -t ${NOM} "

docker build ${OPTS} .
