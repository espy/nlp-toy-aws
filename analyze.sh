#!/usr/bin/env bash
#
# PWD="$(pwd)"
#
NOM="aws"
IMG="espy/aws"

OPTS="${OPTS} -it"
OPTS="${OPTS} --rm"
OPTS="${OPTS} --name ${NOM}2"
OPTS="${OPTS} -v ${PWD}/data:/data"
OPTS="${OPTS} -v ${HOME}/.aws:/root/.aws"
OPTS="${OPTS} -v $(pwd):/aws"

#
mkdir -p data
docker run ${OPTS} ${IMG} python3 /data/analyze.py

