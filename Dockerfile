FROM centos:centos8

ENV LANG C.UTF-8

RUN dnf --assumeyes upgrade
RUN dnf -y install \
    python38 \
    unzip
RUN python3 -m pip install --upgrade pip
RUN pip install \
    boto3
    
# RUN pip install --upgrade requests

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install

CMD ["/bin/bash"]
